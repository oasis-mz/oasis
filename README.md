# Oasis - MZ Front-end test

Observações:

- Neste projeto utilizei o Bootstrap como framework apenas para reaproveitar as classes de flexbox, para tornar o desenvolvimento do front-end mais ágil.
- Busquei utilizar somente funções de JavaScript puro, pesquisei uma boa parte das soluções, porém utilizei as que melhor eu compreendi.
- Utilizei somente bibliotecas de JS puro, buscando sempre ferramentas em ES6.
- Procurei nomear todas as classes de acordo com o padrão BEM, utilizando camelCasing e a língua inglesa.

* Busquei aperfeiçoar minhas técnicas de CSS e aprender novas ferramentas neste projeto, como no caso do Bootstrap 4 e o Glide.js. 